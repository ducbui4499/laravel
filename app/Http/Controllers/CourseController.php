<?php

namespace App\Http\Controllers;

use App\Models\CourseModel;
use Illuminate\Http\Request;
use App\Exports\CourseExport;
use Excel;
use Maatwebsite\Excel\Excel as ExcelExcel;
use Maatwebsite\Excel\Facades\Excel as FacadesExcel;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $course =  CourseModel::orderBy('id', 'desc')->paginate(5);
        return view('course.listCourse', compact('course'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function createCourse()
    {
        return view('course.createCourse');
    }

    public function storeCourse(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'starttime' => 'required',
            'endtime' => 'required'
        ]);
        CourseModel::create($request->all());

        return redirect()->route('listCourse')
            ->with('success', 'Course created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CourseModel  $courseModel
     * @return \Illuminate\Http\Response
     */

    public function showCourse(CourseModel $course)
    {
        return view('course.showCourse', compact('course'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CourseModel  $courseModel
     * @return \Illuminate\Http\Response
     */

    public function editCourse(CourseModel $course)
    {
        return view('course.editCourse', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CourseModel  $courseModel
     * @return \Illuminate\Http\Response
     */

    public function updateCourse(Request $request, CourseModel $course)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $course->update($request->all());


        return redirect()->route('listCourse')
            ->with('success', 'Course Update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CourseModel $userModel
     * @return \Illuminate\Http\Response
     */

    public function destroyCourse(CourseModel $course)
    {
        $course->delete();

        return redirect()->route('listCourse')
            ->with('success', 'Course deleted successfully');
    }

    public function exportExcel()
    {
        return Excel::download(new CourseExport, 'CourseList.xlsx');
    }
    public function exportCSV()
    {
        return Excel::download(new CourseExport, 'courselist.csv');
    }
}
