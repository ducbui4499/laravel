<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class CourseModel extends Model
{
    use HasFactory;
    public $table = 'course';
    protected $fillable = [
        'name',
        'description',
        'starttime',
        'endtime',
    ];
    // public static function getCourse()
    // {
    //     $records = DB::table('course')->select('name'. 'description','starttime','endtime')->get()->toArray();
    //     return $records;
    // }
}
