<?php

namespace Database\Factories;

use App\Models\CourseModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    protected $model = CourseModel::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->name,
            'description' => $this->faker->description,
            'starttime' => $this->faker->starttime,
            'endtime' => $this->faker->starttime

        ];
    }
}
