@extends('page')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>
                            ADD Course
                        </h3>
                    </div>

                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <form action="{{ route('storeCourse') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Course:</strong>
                                <input type="text" name="name" class="form-control" placeholder="Enter Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Description:</strong>
                                <textarea class="form-control" name="description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Start Time:</strong>
                                <input type="date" name="starttime" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>End Time:</strong>
                                <input type="date" name="endtime" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6 text-center">
                            <a class="btn btn-danger float-left" href="{{ url('listCourse') }}"><i
                                    class="fa-sharp fa-solid fa-arrow-left"></i> Back</a>
                            <button type="submit" class="btn btn-primary float-right"> Submit <i
                                    class="fa-sharp fa-solid fa-arrow-right-long"></i></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
