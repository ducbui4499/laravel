@extends('page')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>
                            Edit Course
                        </h3>
                    </div>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <form action="{{ route('updateCourse', $course->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Course:</strong>
                                <input type="text" name="name" value="{{ $course->name }}" class="form-control"
                                    placeholder="Enter Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Description:</strong>
                                <input type="text" name="description" value="{{ $course->description }}"
                                    class="form-control" placeholder="Enter Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Start Time:</strong>
                                <input type="date" name="starttime" value="{{ $course->starttime }}"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>End Time:</strong>
                                <input type="date" name="endtime" value="{{ $course->endtime }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <a class="btn btn-danger float-left" href="{{ url('listCourse') }}"><i
                                    class="fa-sharp fa-solid fa-arrow-left"></i> Back</a>
                            <button type="submit" class="btn btn-primary float-right"> Submit <i
                                    class="fa-sharp fa-solid fa-arrow-right-long"></i></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
