@extends('page')
@section('content')
    <div class="container mt-5">
        <div class="card">
            <div class="card-header ">
                <div class="col-md-6 float-left">
                    <h3> List Course</h3>
                </div>
                <div class="col-auto ">
                    <a href="/exportExcel" class="btn btn-success float-right ml-3">Export <i
                            class="ri-file-excel-2-fill"></i></a>
                </div>
                <div class="col-auto ">
                    <a href="/exportCSV" class="btn btn-warning float-right ml-3">CSV <i
                            class="ri-file-excel-2-fill"></i></a>
                </div>
                <div class="float-right">
                    <a class="btn btn-success" href="{{ url('createCourse') }}"><i class="fa-solid fa-plus"></i> Create New
                        Course</a>
                </div>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p> {{ $message }}</p>
            </div>
        @endif
        <table class="table table-hover table-striped text-center">
            <tr class="bg-info">
                <th>#</th>
                <th>Course</th>
                <th>Description</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($course as $courses)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $courses->name }}</td>
                    <td>{{ $courses->description }}</td>
                    <td>{{ $courses->starttime }}</td>
                    <td>{{ $courses->endtime }}</td>
                    <td>
                        <form action="{{ route('destroyCourse', $courses->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('showCourse', $courses->id) }}"><i
                                    class="fa-solid fa-eye"></i></a>
                            <a class="btn btn-primary"href="{{ route('editCourse', $courses->id) }}"><i
                                    class="fa-sharp fa-solid fa-gear"></i></a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>

                        </form>
                    </td>

                </tr>
            @endforeach
        </table>
        <a class="btn btn-danger" href="{{ url('dashboard') }}"><i class="fa-sharp fa-solid fa-arrow-left"></i> Back</a>
    </div>
@endsection
