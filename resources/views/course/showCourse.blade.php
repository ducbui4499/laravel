@extends('page')
@section('content')
    <div class="container mt-5">
        <div class="card">
            <div class="card-header bg-info">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Show Course</h2>
                    </div>

                </div>
            </div>
            <div class="container mt-4">
                <div class="row">
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>Course:</strong>
                            {{ $course->name }}
                        </div>
                    </div>
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>Description:</strong>
                            {{ $course->description }}
                        </div>
                    </div>
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>Start Time:</strong>
                            {{ $course->starttime }}
                        </div>
                    </div>
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>End Time:</strong>
                            {{ $course->endtime }}
                        </div>
                    </div>

                </div>
                <div class="col-md-6"">
                    <a class="btn btn-danger mb-2" href="{{ url('listCourse') }}"><i
                            class="fa-sharp fa-solid fa-arrow-left"></i> Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
