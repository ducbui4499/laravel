 @extends('layout')

  @section('content')
  <div class="login-box">
    <h2>Login</h2>
    <form action="{{route('login-user')}}" method="post">
        @if(Session::has('success'))
        <div class="alert alert-success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('fail'))
        <div class="alert alert-danger ">{{Session::get('fail')}}</div>
        @endif
        @csrf
      <div class="user-box">

        <input type="text" class="form-control" placeholder=" Email" name="email" value="{{old('email')}}">
        <span class="span text-danger">@error('email') {{$message}} @enderror</span>
      </div>
      <div class="user-box">
        <input type="password" class="form-control" placeholder="Password" name="password" value="">
        <span class="span text-danger">@error('password') {{$message}} @enderror</span>
      </div>
      <button class="btn btn-block btn-primary" type="submit">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        Login</button>

      <br>
      <a href="register">New User !! Register Here.</a>
    </form>
  </div>

  @endsection
