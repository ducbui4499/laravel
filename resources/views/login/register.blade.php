@extends('layout')
@section('content')
    <div class="login-box">
        <h2>Register</h2>
        <form action="{{ route('register-user') }}" method="POST">
            @if (Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('fail'))
                <div class="alert alert-danger ">{{ Session::get('fail') }}</div>
            @endif
            @csrf
            <div class="user-box">

                <input type="text" class="form-control" placeholder="Full Name" name="name" value="{{ old('name') }}">
                <span class="span text-danger">
                    @error('name')
                        {{ $message }}
                    @enderror
                </span>
            </div>
            <div class="user-box">
                <input type="text" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                <span class="span text-danger">
                    @error('email')
                        {{ $message }}
                    @enderror
                </span>
            </div>
            <div class="user-box">
                <input type="password" class="form-control" placeholder="Password" name="password"
                    value="{{ old('password') }}">
                <span class="span text-danger">
                    @error('password')
                        {{ $message }}
                    @enderror
                </span>
            </div>
            <button class="btn btn-block btn-primary" type="submit">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                Register</button>

            <br>
            <a href="login">New User !! Register Here.</a>
        </form>
    </div>
@endsection
