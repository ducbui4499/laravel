@extends('page')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>
                            ADD User
                        </h3>
                    </div>

                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <form action="{{ route('storeUser') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Name:</strong>
                                <input type="text" name="name" class="form-control" placeholder="Enter Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Email:</strong>
                                <input type="text" name="email" class="form-control" placeholder="Enter Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Ngay Sinh:</strong>
                                <input type="date" name="ngaysinh" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Phone:</strong>
                                <input type="text" name="sodt" class="form-control" placeholder="Enter Phone">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Image:</strong>
                                <input type="file" name="image" class="form-control-file">
                            </div>
                        </div>
                        <div class="col-md-6 text-center mt-4">
                            <a class="btn btn-danger float-left" href="{{ url('listUser') }}"><i
                                    class="fa-sharp fa-solid fa-arrow-left"></i> Back</a>
                            <button type="submit" class="btn btn-primary float-right"> Submit <i
                                    class="fa-sharp fa-solid fa-arrow-right-long"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
