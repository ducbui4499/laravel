@extends('page')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>
                            Edit
                        </h3>
                    </div>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <form action="{{ route('updateUser', $userlist->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Name:</strong>
                                <input type="text" name="name" value="{{ $userlist->name }}" class="form-control"
                                    placeholder="Enter Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Email:</strong>
                                <input type="text" name="email" value="{{ $userlist->email }}" class="form-control"
                                    placeholder="Enter Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Ngay Sinh:</strong>
                                <input type="date" name="ngaysinh" value="{{ $userlist->ngaysinh }}"
                                    class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Phone:</strong>
                                <input type="text" name="sodt" class="form-control" value="{{ $userlist->sodt }}"
                                    placeholder="Enter Phone">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Image:</strong>
                                <input type="file" name = "image" class="form-control-file">
                                <br>
                                <img src="/images/{{ $userlist->image }}" width="500px">
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <a class="btn btn-danger" href="{{ url('listUser') }}"><i class="fa-sharp fa-solid fa-arrow-left"></i>  Back</a>
                            <button type="submit" class="btn btn-primary"> Submit <i class="fa-sharp fa-solid fa-arrow-right-long"></i></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
