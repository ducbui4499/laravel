@extends('page')
@section('content')
    <div class="container mt-5">
        <div class="card">
            <div class="card-header d">
                <div class="col-md-6 float-left">
                    <h3> List User</h3>
                </div>

                <div class="con-md-6 float-right">
                    <a class="btn btn-success" href="{{ url('createUser') }}"><i class="fa-solid fa-plus"></i> Create New User</a>
                </div>
            </div>
        </div>


        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p> {{ $message }}</p>
            </div>
        @endif
        <table class="table table-bordered text-center">
            <tr>
                <th>STT</th>
                <th>Name</th>
                <th>Email</th>
                <th>Ngay Sinh</th>
                <th>Phone</th>
                <th>Image</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($userlist as $lists)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $lists->name }}</td>
                    <td>{{ $lists->email }}</td>
                    <td>{{ $lists->ngaysinh }}</td>
                    <td>{{ $lists->sodt }}</td>
                    <td><img src="/images/{{ $lists->image }}" width="100px"></td>
                    <td>
                        <form action="{{ route('destroyUser', $lists->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('showUser', $lists->id) }}"><i
                                    class="fa-solid fa-eye"></i></a>
                            <a class="btn btn-primary"href="{{ route('editUser', $lists->id) }}"><i
                                    class="fa-sharp fa-solid fa-gear"></i></a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-danger" href="{{ url('dashboard') }}"><i class="fa-sharp fa-solid fa-arrow-left"></i> Back</a>
    </div>
@endsection
