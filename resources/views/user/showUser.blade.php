@extends('page')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Show User</h2>
                    </div>

                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {{ $userlist->name }}
                        </div>
                    </div>
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>Email:</strong>
                            {{ $userlist->email }}
                        </div>
                    </div>
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>Ngay Sinh:</strong>
                            {{ $userlist->ngaysinh }}
                        </div>
                    </div>
                    <div class="col-md-6"">
                        <div class="form-group">
                            <strong>Phone:</strong>
                            {{ $userlist->sodt }}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <strong>Image:</strong>
                            <img src="/images/{{ $userlist->image }} " width="500px">
                        </div>
                    </div>
                </div>
                <div class="col-md-6"">
                    <a class="btn btn-danger mb-2" href="{{ url('listUser') }}"><i class="fa-sharp fa-solid fa-arrow-left"></i>  Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
