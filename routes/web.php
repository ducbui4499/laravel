<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login/login');
});

//Login - LogOut -Crud

Route::get('/login',[LoginController::class,'login'])->middleware('alreadyLoggedIn');
Route::get('/register',[LoginController::class,'register'])->middleware('alreadyLoggedIn');
Route::post('/register-user',[LoginController::class,'registerUser'])->name('register-user');
Route::post('/login-user',[LoginController::class,'loginUser'])->name('login-user');
Route::get('/dashboard',[LoginController::class,'dashboard'])->middleware('isLoggedIn');
Route::get('/logout',[LoginController::class,'logout']);
Route::get('/editLogin/{loginId}', [LoginController::class, 'editLogin'])->name('editLogin');
Route::put('/editLogin/{loginId}', [LoginController::class, 'update']) -> name('update');
Route::get('/showLogin/{loginId}', [LoginController:: class, 'showLogin']) ->name('showLogin');


//list USer

Route::get('/listUser', [UserController::class,'index'])->name('listUser');
Route::get('/createUser', [UserController::class,'createUser'])->name('createUser');
Route::post('storeUser/', [UserController::class,'storeUser'])->name('storeUser');
Route::get('showUser/{userlist}', [UserController::class,'showUser'])->name('showUser');
Route::get('editUser/{userlist}', [UserController::class,'editUser'])->name('editUser');
Route::put('editUser/{userlist}', [UserController::class,'updateUser'])->name('updateUser');
Route::delete('listUser/{userlist}', [UserController::class,'destroyUser'])->name('destroyUser');


//Course
Route::get('/listCourse', [CourseController::class,'index'])->name('listCourse');
Route::get('/createCourse', [CourseController::class,'createCourse'])->name('createCourse');
Route::post('storeCourse/', [CourseController::class,'storeCourse'])->name('storeCourse');
Route::get('showCourse/{course}', [CourseController::class,'showCourse'])->name('showCourse');
Route::get('editCourse/{course}', [CourseController::class,'editCourse'])->name('editCourse');
Route::put('editCourse/{course}', [CourseController::class,'updateCourse'])->name('updateCourse');
Route::delete('listCourse/{course}', [CourseController::class,'destroyCourse'])->name('destroyCourse');
Route::get('/exportExcel', [CourseController::class, 'exportExcel'])->name('exportExcel');
Route::get('/exportCSV', [CourseController::class, 'exportCSV'])->name('exportCSV');
